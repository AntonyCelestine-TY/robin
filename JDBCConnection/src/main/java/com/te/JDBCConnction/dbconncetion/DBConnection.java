package com.te.JDBCConnction.dbconncetion;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DBConnection {
	
	
	private static Connection  connection;
	
	private static Properties prop= new Properties();
	
	
	public static Connection getConncetion()  {
		
		
		try {
			FileInputStream fileInputStream = new FileInputStream("db.properties");
			prop.load(fileInputStream);
			connection=DriverManager.getConnection(prop.getProperty("DB_URL"), prop.getProperty("DB_USERNAME"), prop.getProperty("DB_PASSWORD"));
		} catch (IOException | SQLException e) {
			e.printStackTrace();
		}
		
		

		System.out.println("Connection has been established");
		return connection;
		
	}

}
