//package com.Customer;
//
//import static org.junit.jupiter.api.Assertions.assertNotNull;
//
//import org.junit.jupiter.api.Test;
//import org.junit.runner.RunWith;
//import org.mockito.InjectMocks;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import com.Customer.controller.CustomerController;
//import com.Customer.entity.Customer;
//import com.Customer.repository.CustomerRepository;
//
//@RunWith(SpringRunner.class)
//@SpringBootTest
//class CustomerApplicationTests {
//
//	@Autowired
//	private CustomerRepository repository;
//	@InjectMocks
//	private CustomerController controller;
//	private Object objectMapper;
//
//	@Test
//	public void testCreate() {
//		Customer customer = new Customer();
//		customer.setId(1);
//		customer.setName("Celestine");
//		customer.setAge(24);
//		// repository.save(customer);
//		controller.saveCustomer(customer);
//		assertNotNull(repository.findById(1).get());
//
//	}
//	
//	
//	@Test
//	public void testAddedEmployee() throws Exception {
//		Wrapper wrapper = Wrapper.builder().employee(Employee.builder().firstname("hrushi").build()).build();
//		String AsString = objectMapper.writeValueAsString(wrapper);
//
//		when(services.addemployee(Mockito.any())).thenReturn(wrapper);
//		String contentAsString = mockmvc
//				.perform(post("/addemployee").contentType(MediaType.APPLICATION_JSON_VALUE).content(AsString))
//				.andExpect(status().isOk()).andReturn().getResponse() // .getContentType();
//				.getContentAsString();
//		Responce response = objectMapper.readValue(contentAsString, Responce.class);
//		@SuppressWarnings("unchecked")
//		Map<String, Object> map = (Map<String, Object>) response.getData();
//		@SuppressWarnings("unchecked")
//		Map<String, String> map1 = (Map<String, String>) map.get("employee");
//		assertEquals(wrapper.getEmployee().getFirstname(), map1.get("firstname"));
//	}
//
//}
