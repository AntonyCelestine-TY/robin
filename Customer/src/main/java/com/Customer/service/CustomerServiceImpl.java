package com.Customer.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.Customer.entity.Customer;
import com.Customer.repository.CustomerRepository;

@Service
public class CustomerServiceImpl implements Customerservice {
	
	
	@Autowired
	private CustomerRepository customerRepository;

	@Override
	public Customer addCustomer(Customer customer) {
		// TODO Auto-generated method stub
		return customerRepository.save(customer);
	}

	@Override
	public Customer getCustomer(Integer id) {
		// TODO Auto-generated method stub
		return customerRepository.findById(id).get();
	}

	@Override
	public Customer updateCustomer(Customer customer,Integer id) {
		Optional<Customer> findById = customerRepository.findById(id);
		
		if(findById.isPresent()) {
			//Optional<Customer> findById2 = customerRepository.findById(id);
			//Customer customer = findById2.get();
			customer.setAge(customer.getAge());
			customer.setId(customer.getId());
			customer.setName(customer.getName());
			//customer customer=new Customer();
		return	customerRepository.save(customer);
		}else {
			
			return customerRepository.save(customer);
		}
		
	}

}
