package com.Customer.service;

import com.Customer.entity.Customer;

public interface Customerservice {

	
	
	public Customer addCustomer(Customer customer);
	public Customer getCustomer(Integer id);
	
	public Customer updateCustomer( Customer customer, Integer id);
}
