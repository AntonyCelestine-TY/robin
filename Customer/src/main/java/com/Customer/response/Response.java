package com.Customer.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Response {
	
	
	private boolean error;
	
	private String message;
	
	private Object data;

}
