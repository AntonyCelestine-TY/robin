package com.Customer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.Customer.entity.Customer;
import com.Customer.response.Response;
import com.Customer.service.Customerservice;

@RestController
@RequestMapping("/customer")
public class CustomerController {
	
	
	@Autowired
	private Customerservice customerservice;
	
	@PostMapping("/post")
	public ResponseEntity<Response> saveCustomer(@RequestBody Customer customer){
		Customer addCustomer = customerservice.addCustomer(customer);
		return new ResponseEntity<Response>(new Response(false, "Customer info saved successsfully", addCustomer),HttpStatus.OK);
		
	}
	
	
	@GetMapping("/get/{id}")
	public ResponseEntity<Response> getCustomer(@PathVariable Integer id){
		Customer addCustomer = customerservice.getCustomer(id);
		return new ResponseEntity<Response>(new Response(false, "Customer info fetched successsfully", addCustomer),HttpStatus.OK);
		
	}
	@PostMapping("/{id}")
	public ResponseEntity<Response> updateCustomer(@RequestBody Customer customer,@PathVariable Integer id){
		Customer addCustomer = customerservice.updateCustomer(customer,id);
		return new ResponseEntity<Response>(new Response(false, "Customer info updated successsfully", addCustomer),HttpStatus.OK);
		
	}
	
	
	

}
